#!/bin/bash
set -eu

: "${runsvdir:=runsvdir}"
runsvpath=$(dirname $PWD)

rm -rf timestamp/supervise/ service/
rm -f ./timestamp.txt

mkdir service

#runsvdir need to know where to find runsv
PATH=$runsvpath:$PATH "${runsvdir}" ./service &
echo "pid = $!"
# Wait till runsvdir scans directory and concludes it is empty
sleep 1 
now=$(date +%s%N)
echo "now = ${now}"
ln -sf ../timestamp service/
kill -ALRM $!

# Wait until runsvdir discovers "service/timestamp" symbolic link
# and is ready to accept commands on control pipe.
while ! test -e timestamp/supervise/control ; do
    sleep 0.001
done

new=$(date +%s%N)
echo "new = ${new}"
kill -HUP $!

diff=$((new - now))
echo "diff = ${diff}"

if [[ ${diff} -lt 1000000000 ]] ; then
    echo "fine. forced-rescan seems to work"
    rm -rf timestamp/supervise/ service/ || true
    exit 0
else
    echo "bad. forced-rescan took too long"
    rm -rf timestamp/supervise/ service/ || true
    exit 1
fi
    
